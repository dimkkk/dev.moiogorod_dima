<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Торговая площадка Мой огород");
$APPLICATION->SetTitle("Добавить объявление");?>


<h1>Подать объявление</h1>

<div class="contact-info-add col-lg-8 col-md-8 col-sm-8 col-xs-12">
  <h4>Контактная информация</h4>

  <div class="add-email">
    <p>Электронная почта</p>
    <p>celldweller24@gmail.com</p>
  </div>
  <div class="add-name">
    <p>Ваше имя</p>
    <p>Nikita Chubukov</p>
  </div>
  <div class="add-phone">
    <p>Телефон</p>
    <p><input type="text" class="form-control" placeholder="Введите номер телефона"></p>
  </div>
</div>
<div class="add-policy col-lg-4 col-md-4 col-sm-4 col-xs-12">
  <div>
    <p>Все объявления должны соответствовать праилам сайта</p>
    <p>Не подавайте одно и то же объявление повторно.</p>
    <p>Не указывайте телефон, электронную почту или адрес сайта в описании или на фото</p>
    <p>Не предлагайте запрещённые товары и услуги</p>
  </div>
</div>
<div class="row">
  <div class="adding-form-wrapper col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="add-category">
      <h4>Выберите категорию</h4>
      <p>
        <select class="selectpicker">
          <option>Животноводство</option>
          <option>Растениеводство</option>
          <option>Готовая продукция</option>
          <option>Сельхозтехника</option>
          <option>Недвижимость</option>
          <option>Хозяйство</option>
        </select>
      </p>
    </div>
    <div class="add-address">
      <h4>Местоположение</h4>
      <div>
        <p class="form-item-title">Город</p>
        <p>
          <select class="selectpicker" data-live-search="true">
            <option>Москва</option>
            <option>Санкт-Петербург</option>
            <option>Краснодар</option>
            <option>Омск</option>
            <option>Магадан</option>
            <option>Петрозаводск</option>
          </select>
        </p>
      </div>
      <div>
        <p class="form-item-title">Метро</p>
        <p>
          <select class="selectpicker">
            <option>Автозаводская</option>
            <option>Академическая</option>
            <option>Бабушкинская</option>
            <option>Беляево</option>
            <option>Теплый стан</option>
          </select>
        </p>
      </div>
      <div>
        <p class="form-item-title">Адрес</p>
        <p><input type="text" class="form-control" placeholder="Введите номер телефона"></p>
      </div>
    </div>
    <div class="add-options">

    </div>
  </div>
  <div class="add-banner col-lg-4 col-md-4 col-sm-4 col-xs-12">

  </div>
</div>

<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>