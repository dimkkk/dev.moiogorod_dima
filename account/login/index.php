<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
checkAuthRedirect();
$APPLICATION->SetTitle("Вход");
?><?$APPLICATION->IncludeComponent(
	"bitrix:system.auth.form", 
	"login.form", 
	array(
		"FORGOT_PASSWORD_URL" => "/account/forgot-password/",
		"PROFILE_URL" => "/account/advertisement/",
		"REGISTER_URL" => "/account/registration/",
		"SHOW_ERRORS" => "Y",
		"COMPONENT_TEMPLATE" => "login.form",
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>