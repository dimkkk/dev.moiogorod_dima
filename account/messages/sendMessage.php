<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;
use Bitrix\Main\Type\DateTime;

$messages_block_id = 2;
$entity_data_class = GetEntityDataClass($messages_block_id);

if (!empty($_POST)) {
	if (!empty($_POST['backurl'])) {
		$data = array(
	            "UF_USER_ID_FROM" => $_POST['user_id_from'],
	            "UF_USER_ID_TO" => $_POST['user_id_to'],
	            "UF_PRODUCT_ID" => $_POST['item_id'],
	            'UF_TYPE' => 'user',
	            //'UF_TITLE' => ,
	            'UF_BODY' => $_POST['review'],
	            'UF_IS_READ' => false,
	            'UF_IS_DELETED_TO' => false,
	            'UF_IS_DELETED_FROM' => false,
	            'UF_TIMESTAMP' => new DateTime()
	        );
		$result = $entity_data_class::add($data);
		header('Location:'. $_POST['backurl']);
	} else {
		$data = array(
	            "UF_USER_ID_FROM" => $_POST['senderId'],
	            "UF_USER_ID_TO" => $_POST['receiver'],
	            "UF_PRODUCT_ID" => $_POST['productId'],
	            'UF_TYPE' => 'user',
	            //'UF_TITLE' => ,
	            'UF_BODY' => $_POST['messageText'],
	            'UF_IS_READ' => false,
	            'UF_IS_DELETED_TO' => false,
	            'UF_IS_DELETED_FROM' => false,
	            'UF_TIMESTAMP' => new DateTime()
	        );
		$result = $entity_data_class::add($data);
	}
}
?>
