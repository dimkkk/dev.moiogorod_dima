<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if ($arResult['FORM_TYPE'] == 'logout') : ?>
  <? LocalRedirect('/')?>
<? endif; ?>
<?/* $curentUser = CUser::GetByID(CUser::GetID())->Fetch(); */?>
<div class="bx-system-auth-form account-page">
  <? if ($arResult["FORM_TYPE"] == "login"): ?>
    <div
      class="login-form-wrapper col-lg-6 col-md-6 col-sm-10 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-1">
      <h2>Вход</h2>
      <?
      if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
        ShowMessage($arResult['ERROR_MESSAGE']);
      ?>
      <form name="system_auth_form<?= $arResult["RND"] ?>" method="post" target="_top"
            action="<?= $arResult["AUTH_URL"] ?>">
        <? if ($arResult["BACKURL"] <> ''): ?>
          <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
        <?endif ?>
        <? foreach ($arResult["POST"] as $key => $value): ?>
          <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
        <?endforeach ?>
        <input type="hidden" name="AUTH_FORM" value="Y"/>
        <input type="hidden" name="TYPE" value="AUTH"/>
        <div class="auth-form">
          <p class="user-login-field">
            <input type="text" name="USER_LOGIN" class="form-control" value="<?= $arResult["USER_LOGIN"] ?>"
                   placeholder="Электронаая почта"/>
          </p>
          <p class="password-login-field">
            <input type="password" name="USER_PASSWORD" class="form-control" autocomplete="off" placeholder="Пароль"/>
            <? if ($arResult["SECURE_AUTH"]): ?>
            <span class="bx-auth-secure" id="bx_auth_secure<?= $arResult["RND"] ?>"
                  title="<? echo GetMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
						<div class="bx-auth-secure-icon"></div>
					</span>
          <noscript>
						<span class="bx-auth-secure" title="<? echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
							<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
						</span>
          </noscript>
          <script type="text/javascript">
            document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
          </script>
        <?endif ?>
          </p>
          <div class="help-login">
            <?/*if ($arResult["STORE_PASSWORD"] == "Y"):?>
				<div class="remember-me col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<p><input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" /></p>
					<p><label for="USER_REMEMBER_frm" title="<?=GetMessage("AUTH_REMEMBER_ME")?>"><?echo GetMessage("AUTH_REMEMBER_SHORT")?></label></p>
				</div>
			<?endif*/ ?>
            <? if ($arResult["NEW_USER_REGISTRATION"] == "Y"): ?>
              <div class="forgot-pass col-sm-6 col-xs-12 col-sm-push-6">
                <p>
                  <a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>"
                     rel="nofollow"><?= GetMessage("AUTH_FORGOT_PASSWORD_2") ?></a>
                </p>
              </div>
            <?endif; ?>
          </div>
          <? if ($arResult["CAPTCHA_CODE"]): ?>
            <div class="login-capcha">
              <? echo GetMessage("AUTH_CAPTCHA_PROMT") ?>:<br/>
              <input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>"/>
              <img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>" width="180"
                   height="40" alt="CAPTCHA"/><br/><br/>
              <input type="text" name="captcha_word" maxlength="50" value=""/>
            </div>
          <?endif ?>
          <p class="button-login">
            <input type="submit" name="Login" class="enter-button" value="<?= GetMessage("AUTH_LOGIN_BUTTON") ?>"/>
          </p>
          <p class="or-line">или</p>
          <? if ($arResult["NEW_USER_REGISTRATION"] == "Y"): ?>
            <div>
              <p class="register-button">
                <a href="<?= $arResult["AUTH_REGISTER_URL"] ?>" rel="nofollow"><?= GetMessage("AUTH_REGISTER") ?></a>
              </p>
            </div>
          <?endif ?>
          <? if ($arResult["AUTH_SERVICES"]): ?>
            <div>
              <div class="bx-auth-lbl"><?= GetMessage("socserv_as_user_form") ?></div>
              <?
              $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "icons",
                array(
                  "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                  "SUFFIX" => "form",
                ),
                $component,
                array("HIDE_ICONS" => "Y")
              );
              ?>
            </div>
          <?endif ?>
        </div>
      </form>
    </div>

    <? if ($arResult["AUTH_SERVICES"]): ?>
      <?
      $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
        array(
          "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
          "AUTH_URL" => $arResult["AUTH_URL"],
          "POST" => $arResult["POST"],
          "POPUP" => "Y",
          "SUFFIX" => "form",
        ),
        $component,
        array("HIDE_ICONS" => "N")
      );
      ?>

      <? $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "icons",
        array(
          "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
          "SUFFIX" => "form",
        ),
        $component,
        array("HIDE_ICONS" => "Y")
      ); ?>
    <?endif ?>


    <?
  elseif ($arResult["FORM_TYPE"] == "otp"):
    ?>

    <form name="system_auth_form<?= $arResult["RND"] ?>" method="post" target="_top"
          action="<?= $arResult["AUTH_URL"] ?>">
      <? if ($arResult["BACKURL"] <> ''):?>
        <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
      <?endif ?>
      <input type="hidden" name="AUTH_FORM" value="Y"/>
      <input type="hidden" name="TYPE" value="OTP"/>
      <table width="95%">
        <tr>
          <td colspan="2">
            <? echo GetMessage("auth_form_comp_otp") ?><br/>
            <input type="text" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off"/></td>
        </tr>
        <? if ($arResult["CAPTCHA_CODE"]):?>
          <tr>
            <td colspan="2">
              <? echo GetMessage("AUTH_CAPTCHA_PROMT") ?>:<br/>
              <input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>"/>
              <img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>" width="180"
                   height="40" alt="CAPTCHA"/><br/><br/>
              <input type="text" name="captcha_word" maxlength="50" value=""/></td>
          </tr>
        <?endif ?>
        <? if ($arResult["REMEMBER_OTP"] == "Y"):?>
          <tr>
            <td valign="top"><input type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER" value="Y"/></td>
            <td width="100%"><label for="OTP_REMEMBER_frm"
                                    title="<? echo GetMessage("auth_form_comp_otp_remember_title") ?>"><? echo GetMessage("auth_form_comp_otp_remember") ?></label>
            </td>
          </tr>
        <?endif ?>
        <tr>
          <td colspan="2"><input type="submit" name="Login" value="<?= GetMessage("AUTH_LOGIN_BUTTON") ?>"/></td>
        </tr>
        <tr>
          <td colspan="2">
            <noindex><a href="<?= $arResult["AUTH_LOGIN_URL"] ?>"
                        rel="nofollow"><? echo GetMessage("auth_form_comp_auth") ?></a></noindex>
            <br/></td>
        </tr>
      </table>
    </form>

    <?
  else:
    ?>

    <form action="<?= $arResult["AUTH_URL"] ?>">
      <table width="95%">
        <tr>
          <td align="center">
            <p class="auth-hello">Вы авторизованы в системе</p>
            <a href="<?= $arResult["PROFILE_URL"] ?>"
               title="<?= GetMessage("AUTH_PROFILE") ?>"><?= GetMessage("AUTH_PROFILE") ?></a><br/>
          </td>
        </tr>
        <tr>
          <td align="center">
            <? foreach ($arResult["GET"] as $key => $value):?>
              <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
            <? endforeach ?>
            <input type="hidden" name="logout" value="yes"/>
            <input type="submit" name="logout_butt" value="<?= GetMessage("AUTH_LOGOUT_BUTTON") ?>"/>
          </td>
        </tr>
      </table>
    </form>
  <? endif ?>
</div>