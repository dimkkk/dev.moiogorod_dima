<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult["ALL_ITEMS"]))
  return;

if (file_exists($_SERVER["DOCUMENT_ROOT"] . $this->GetFolder() . '/themes/' . $arParams["MENU_THEME"] . '/colors.css'))
  $APPLICATION->SetAdditionalCSS($this->GetFolder() . '/themes/' . $arParams["MENU_THEME"] . '/colors.css');

CJSCore::Init();

$menuBlockId = "catalog_menu_" . $this->randString();
?>
<div class="main-left-wrapper">
  <ul>
    <? foreach ($arResult["MENU_STRUCTURE"] as $itemID => $arColumns): ?>     <!-- first level-->
      <li class="main-left-item <? if ($arResult["ALL_ITEMS"][$itemID]["SELECTED"]): ?>current<? endif ?>">
        <div class="first-level-link">
          <a class="main-left-section-link"
             <?if ($arResult['ALL_ITEMS'][$itemID]['IS_PARENT']) :?>
               role="button"
             <?else :?>
               <a href="<?= $arResult["ALL_ITEMS"][$itemID]["LINK"]?>"
             <?endif;?> data-toggle="collapse" data-target="#<?=$arParams['PREFIX_MENU']?>-menu-id<?=$itemID?>">
            <?= $arResult["ALL_ITEMS"][$itemID]["TEXT"]?>
          </a>
          <?if ($arResult['ALL_ITEMS'][$itemID]['IS_PARENT']) :?>
            <a class="menu-item-arrow" role="button" data-toggle="collapse" data-target="#<?=$arParams['PREFIX_MENU']?>-menu-id<?=$itemID?>" aria-expanded="false">
              <img src="<?=SITE_TEMPLATE_PATH?>/images/menu_arrow.png">
            </a>
          <?endif;?>
        </div>
        <? if (is_array($arColumns) && count($arColumns) > 0): ?>
          <div id='<?=$arParams['PREFIX_MENU']?>-menu-id<?=$itemID?>' class="main-left-nested-wrapper collapse">
            <? foreach ($arColumns as $key => $arRow): ?>
              <div class="main-left-child">
                <ul>
                  <? foreach ($arRow as $itemIdLevel_2 => $arLevel_3): ?>  <!-- second level-->
                    <li class="parent">
                      <a href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"] ?>">
                        <?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"] ?>
                      </a>
                      <? if (is_array($arLevel_3) && count($arLevel_3) > 0): ?>
                        <ul>
                          <? foreach ($arLevel_3 as $itemIdLevel_3): ?>  <!-- third level-->
                            <li>
                              <a href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"] ?>"
                                <?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["TEXT"] ?>
                              </a>
                              <span style="display: none">
                                <?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["PARAMS"]["description"] ?>
                              </span>
                              <span class="bx_children_advanced_panel">
                                <img src="<?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["PARAMS"]["picture_src"] ?>" alt="">
                              </span>
                            </li>
                          <? endforeach; ?>
                        </ul>
                      <? endif ?>
                    </li>
                  <? endforeach; ?>
                </ul>
              </div>
            <? endforeach; ?>
            <div style="clear: both;"></div>
          </div>
        <? endif ?>
      </li>
    <? endforeach; ?>
  </ul>
  <div style="clear: both;"></div>
</div>