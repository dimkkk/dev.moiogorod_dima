<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="reviews-list">
<?if(count($arResult["ITEMS"])):?>
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="review-item <?=$arItem["PROPERTIES"]["USER_ID"]["VALUE"] === CUser::GetID()?'owner':''?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <p class="title">
                <?
                $rsUser = CUser::GetByID($arItem["PROPERTIES"]["USER_ID"]["VALUE"]);
                $arUser = $rsUser->Fetch();
                ?>
                <span class="user-name"><?=$arUser["NAME"]?></span>
                <?if($arItem["PROPERTIES"]["RAITING"]["VALUE"] > 0):?>
                    <span class="stars">
                        <?
                        for($i = 0; $i<$arItem["PROPERTIES"]["RAITING"]["VALUE"]; $i++){
                            ?><i class="fa fa-star active"></i><?
                        }
                        for($i = $arItem["PROPERTIES"]["RAITING"]["VALUE"]; $i < 5; $i++){
                            ?><i class="fa fa-star"></i><?
                        }
                        ?>
                    </span>
                <?endif?>
            </p>
            <p class="review-text"><?
                $tags = array('[quote]', '[/quote]', '[b]', '[/b]');
                $replaceTags = array('<span class="quote">', '</span><span class="answer">', '<strong>', '</strong><br>');
                $reviewText = str_replace($tags, $replaceTags, $arItem["PROPERTIES"]["TEXT_REVIEW"]["VALUE"], $changeCount);
                if($changeCount == 4){
                    echo nl2br('<span class=\'with-quote\'>'.$reviewText.'</span></span>');
                }
                else{
                    echo nl2br(strip_tags($reviewText));
                }
                ?></p>
            <p class="date-answer">
                <span class="date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
                <?if($arParams['AD_CREATED_BY_ID'] === CUser::GetID() && CUser::GetID() != $arItem["PROPERTIES"]["USER_ID"]["VALUE"]):?>
                    <span class="answer-button">
                        Ответить
                    </span>
                <?endif;?>
            </p>
        </div>
    <?endforeach;?>
<?else:?>
    <div class="review-item">
        <p>Ваш комментарий будет первым</p>
    </div>
<?endif?>
</div>
