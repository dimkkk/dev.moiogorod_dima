<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="left-menu-section col-lg-3 col-md-3 hidden-sm hidden-xs">
  <? $APPLICATION->IncludeComponent(
    "bitrix:menu",
    "main.menu.left",
    Array(
      "ALLOW_MULTI_SELECT" => "N",
      "CHILD_MENU_TYPE" => "main",
      "COMPONENT_TEMPLATE" => "catalog_vertical",
      "DELAY" => "N",
      "MAX_LEVEL" => "2",
      "MENU_CACHE_GET_VARS" => "",
      "MENU_CACHE_TIME" => "3600",
      "MENU_CACHE_TYPE" => "N",
      "MENU_CACHE_USE_GROUPS" => "Y",
      "MENU_THEME" => "site",
      "ROOT_MENU_TYPE" => "main",
      "USE_EXT" => "N",
      "PREFIX_MENU" => "section"
    )
  ); ?>
</div>

<div class="search-page col-lg-6 col-md-6">
  <h1>Результаты поиска</h1>
  <? if (isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
    ?>
    <div class="search-language-guess">
      <? echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#" => '<a href="' . $arResult["ORIGINAL_QUERY_URL"] . '">' . $arResult["REQUEST"]["ORIGINAL_QUERY"] . '</a>')) ?>
    </div><br/><?
  endif; ?>

  <? if ($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false): ?>
  <? elseif ($arResult["ERROR_CODE"] != 0): ?>
    <p><?= GetMessage("SEARCH_ERROR") ?></p>
    <? ShowError($arResult["ERROR_TEXT"]); ?>
    <p><?= GetMessage("SEARCH_CORRECT_AND_CONTINUE") ?></p>
    <br/><br/>
    <p><?= GetMessage("SEARCH_SINTAX") ?><br/><b><?= GetMessage("SEARCH_LOGIC") ?></b></p>
    <table border="0" cellpadding="5">
      <tr>
        <td align="center" valign="top"><?= GetMessage("SEARCH_OPERATOR") ?></td>
        <td valign="top"><?= GetMessage("SEARCH_SYNONIM") ?></td>
        <td><?= GetMessage("SEARCH_DESCRIPTION") ?></td>
      </tr>
      <tr>
        <td align="center" valign="top"><?= GetMessage("SEARCH_AND") ?></td>
        <td valign="top">and, &amp;, +</td>
        <td><?= GetMessage("SEARCH_AND_ALT") ?></td>
      </tr>
      <tr>
        <td align="center" valign="top"><?= GetMessage("SEARCH_OR") ?></td>
        <td valign="top">or, |</td>
        <td><?= GetMessage("SEARCH_OR_ALT") ?></td>
      </tr>
      <tr>
        <td align="center" valign="top"><?= GetMessage("SEARCH_NOT") ?></td>
        <td valign="top">not, ~</td>
        <td><?= GetMessage("SEARCH_NOT_ALT") ?></td>
      </tr>
      <tr>
        <td align="center" valign="top">( )</td>
        <td valign="top">&nbsp;</td>
        <td><?= GetMessage("SEARCH_BRACKETS_ALT") ?></td>
      </tr>
    </table>
  <? elseif (count($arResult["SEARCH"]) > 0): ?>
    <div class="item-list">
      <? foreach ($arResult['SEARCH'] as $key => $arItem) : ?>
        <?
        $picturesId = array();
        $res = CIBlockElement::GetProperty(1, $arItem['ITEM_ID'], "sort", "asc", array("CODE" => "ADDITIONAL_PHOTO"));
        while ($ob = $res->GetNext()) {
          $picturesId[] = $ob['VALUE'];
        }

        $picture = CFile::ResizeImageGet($picturesId[0], ['width' => '165', 'height' => '180'], BX_RESIZE_IMAGE_EXACT, true, [], false, "80"); ?>
        <div id="<? echo $this->GetEditAreaId($arItem['ID']); ?>" class="element-item">
          <div class="item-image col-xs-4">
            <a href="<?= $arItem['URL_WO_PARAMS'] . $arItem['ITEM_ID'] . '/' ?>">
              <img
                src="<?= (!empty($picture)) ? $picture['src'] : SITE_TEMPLATE_PATH . '/images/no_image_photo.png' ?>">
            </a>
          </div>
          <div class="item-addinfo col-xs-8">
            <p class="item-title">
              <a href="<?= $arItem['URL_WO_PARAMS'] . $arItem['ITEM_ID'] . '/' ?>">
                <?= TruncateText(strip_tags($arItem['TITLE']), 90) ?>
              </a>
            </p>
            <!--<p class="element-price"><? /*=$arItem['PROPERTIES']['ITEM_COST']['VALUE'] . ' руб.'*/ ?></p>-->
            <!--<p class="element-price"><? /*= CurrencyFormat($arItem['PROPERTIES']['ITEM_COST']['VALUE'], "RUB"); */ ?></p>-->
            <p class="item-description">
              <?= TruncateText(strip_tags($arItem['BODY']), 140) ?>
            </p>
            <div class="item-distance-date">
              <p class="publication-date"><?= getPublicationDate($arItem['DATE_FROM']) ?></p>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      <? endforeach; ?>
    </div>

    <? /* if ($arParams["DISPLAY_TOP_PAGER"] != "N") echo $arResult["NAV_STRING"] */ ?><!--
    <br/>
    <hr/>
    <? /* foreach ($arResult["SEARCH"] as $arItem): */ ?>
      <a href="<? /* echo $arItem["URL"] */ ?>"><? /* echo $arItem["TITLE_FORMATED"] */ ?></a>
      <p><? /* echo $arItem["BODY_FORMATED"] */ ?></p>
      <? /* if (
        $arParams["SHOW_RATING"] == "Y"
        && strlen($arItem["RATING_TYPE_ID"]) > 0
        && $arItem["RATING_ENTITY_ID"] > 0
      ): */ ?>
        <div class="search-item-rate"><? /*
          $APPLICATION->IncludeComponent(
            "bitrix:rating.vote", $arParams["RATING_TYPE"],
            Array(
              "ENTITY_TYPE_ID" => $arItem["RATING_TYPE_ID"],
              "ENTITY_ID" => $arItem["RATING_ENTITY_ID"],
              "OWNER_ID" => $arItem["USER_ID"],
              "USER_VOTE" => $arItem["RATING_USER_VOTE_VALUE"],
              "USER_HAS_VOTED" => $arItem["RATING_USER_VOTE_VALUE"] == 0 ? 'N' : 'Y',
              "TOTAL_VOTES" => $arItem["RATING_TOTAL_VOTES"],
              "TOTAL_POSITIVE_VOTES" => $arItem["RATING_TOTAL_POSITIVE_VOTES"],
              "TOTAL_NEGATIVE_VOTES" => $arItem["RATING_TOTAL_NEGATIVE_VOTES"],
              "TOTAL_VALUE" => $arItem["RATING_TOTAL_VALUE"],
              "PATH_TO_USER_PROFILE" => $arParams["~PATH_TO_USER_PROFILE"],
            ),
            $component,
            array("HIDE_ICONS" => "Y")
          ); */ ?>
        </div>
      <? /* endif; */ ?>
      <small><? /*= GetMessage("SEARCH_MODIFIED") */ ?> <? /*= $arItem["DATE_CHANGE"] */ ?></small><br/><? /*
      if ($arItem["CHAIN_PATH"]):*/ ?>
        <small><? /*= GetMessage("SEARCH_PATH") */ ?>&nbsp;<? /*= $arItem["CHAIN_PATH"] */ ?></small><? /*
      endif;
      */ ?>
      <hr/>
    <? /* endforeach; */ ?>
    <? /* if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"] */ ?>
    <br/>
    <p>
      <? /* if ($arResult["REQUEST"]["HOW"] == "d"): */ ?>
        <a
          href="<? /*= $arResult["URL"] */ ?>&amp;how=r<? /* echo $arResult["REQUEST"]["FROM"] ? '&amp;from=' . $arResult["REQUEST"]["FROM"] : '' */ ?><? /* echo $arResult["REQUEST"]["TO"] ? '&amp;to=' . $arResult["REQUEST"]["TO"] : '' */ ?>"><? /*= GetMessage("SEARCH_SORT_BY_RANK") */ ?></a>&nbsp;|&nbsp;
        <b><? /*= GetMessage("SEARCH_SORTED_BY_DATE") */ ?></b>
      <? /* else: */ ?>
        <b><? /*= GetMessage("SEARCH_SORTED_BY_RANK") */ ?></b>&nbsp;|&nbsp;<a
          href="<? /*= $arResult["URL"] */ ?>&amp;how=d<? /* echo $arResult["REQUEST"]["FROM"] ? '&amp;from=' . $arResult["REQUEST"]["FROM"] : '' */ ?><? /* echo $arResult["REQUEST"]["TO"] ? '&amp;to=' . $arResult["REQUEST"]["TO"] : '' */ ?>"><? /*= GetMessage("SEARCH_SORT_BY_DATE") */ ?></a>
      <? /* endif; */ ?>
    </p>-->
  <? else: ?>
    <? ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND")); ?>
  <? endif; ?>
</div>

<div class="vip-advert-wrapper col-lg-3 col-md-3 col-xs-12">
  <div class="vip-items">
    <? $APPLICATION->IncludeComponent(
      "bitrix:main.include",
      ".default",
      array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "AREA_FILE_RECURSIVE" => "Y",
        "EDIT_TEMPLATE" => "standard.php",
        "COMPONENT_TEMPLATE" => ".default",
        "PATH" => "/includes/vip_ads.php"
      ),
      false
    ); ?>
  </div>
</div>