<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
/* Get user parameters*/
$userId = $arResult['CREATED_BY'];
$curentUser = CUser::GetByID($userId)->Fetch();

/* Get detail pictures */
$resultPictures = [];
foreach ($arResult['PROPERTIES']['ADDITIONAL_PHOTO']['VALUE'] as $itemPicture) {
  $resultPictures[] = array(
    'resised_picture' => CFile::ResizeImageGet($itemPicture, array('width' => '500', 'height' => '400'), BX_RESIZE_IMAGE_EXACT, true, array(), false, "80"),
    'original_picture' => CFile::ResizeImageGet($itemPicture, array('width' => '100%', 'height' => 'auto'), BX_RESIZE_IMAGE_EXACT, true, array(), false, "80"),
  );
}
?>

<!--<div class="publication-info">
  <p class="publication-date"><?/*= getPublicationDate($arResult['DATE_ACTIVE_FROM']) */?></p>
  <p class="views-item">
    <?/* if (CModule::IncludeModule("iblock")) {
      CIBlockElement::CounterInc($arResult['ID']);
      $res = CIBlockElement::GetByID($arResult['ID']);
    }
    if ($ar_res = $res->GetNext()) {
      echo 'Количество просмотров: ' . $ar_res['SHOW_COUNTER'];
    } */?>
  </p>
</div>-->

<h1><?= $arResult['NAME'] ?></h1>
<div class="row element-info">
    <div class="col-md-6 slider-block">
        <div class="main-img">
            <? if (count($resultPictures) >= 1) :?>
                <?$pictureSmall = CFile::ResizeImageGet($arResult['PROPERTIES']['ADDITIONAL_PHOTO']['VALUE'][0], array('width'=>'270', 'height'=>'300'), BX_RESIZE_IMAGE_EXACT, true);?>
                <?$pictureBig = CFile::ResizeImageGet($arResult['PROPERTIES']['ADDITIONAL_PHOTO']['VALUE'][0], array('width'=>'1024', 'height'=>'768'), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
                <a class="fancybox-detail" rel="gallery" href="<?= $pictureBig['src'] ?>">
                    <img src="<?=$pictureSmall['src'] ?>" alt="<?=$arResult['NAME']?>">
                </a>
            <? else :?>
                <img src="<?= SITE_TEMPLATE_PATH ?>/images/no_image_photo.png">
            <? endif; ?>
        </div>
    </div>
    <div class="col-md-6 properties-block">
        <div class="item-info-block">
            <? if (!empty($arResult['PROPERTIES']['ITEM_COST']['VALUE'])) : ?>
                <div class="item-price">
                    <p class="price"><?= CurrencyFormat($arResult['PROPERTIES']['ITEM_COST']['VALUE'], "RUB"); ?></p>
                    <? if ($arResult['PROPERTIES']['AUCTION']['VALUE']) : ?>
                        <p class="item-auction">Возможен торг</p>
                    <? endif; ?>
                </div>
            <? else : ?>
                <div class="item-price">Не указана</div>
            <? endif; ?>

            <? /*User name*/
            if (!empty($curentUser['NAME'])) :?>
                <div class="item-vendor-name specification">
                    <p><?= $curentUser['NAME'] ?></p>
                </div>
            <? endif; ?>

            <? /*User phone*/
            if (!empty($arResult['PROPERTIES']['ADDTIONAL_PHONE']['VALUE'])) :?>
                <div class="item-vendor-phone specification">
                    <!--<p class="prevent-button">
                      <button type="button" class="btn btn-success">Показать номер</button>
                    </p>-->
                    <p><?= $arResult['PROPERTIES']['ADDTIONAL_PHONE']['VALUE'] ?></p>
                </div>
            <? endif; ?>

            <? /* City place */

            if (!empty($arResult['PROPERTIES']['VENDOR_CITY']['VALUE'])) :?>
                <div class="item-vendor-city specification">
                    <p>
                        <?php
                            echo $arResult['PROPERTIES']['VENDOR_AREA']['VALUE'];
                            echo $arResult['PROPERTIES']['VENDOR_SUBAREA']['VALUE']? ', '.$arResult['PROPERTIES']['VENDOR_SUBAREA']['VALUE']:'';
                            echo $arResult['PROPERTIES']['VENDOR_CITY']['VALUE']? ', '.$arResult['PROPERTIES']['VENDOR_CITY']['VALUE']:'';
                        ?>
                    </p>
                </div>
            <? endif; ?>

            <? /* Product unit */
            if (!empty($arResult['PROPERTIES']['AMOUNT_TYPE']['VALUE'])) :?>
                <div class="item-amount specification">
                    <p><?= $arResult['PROPERTIES']['AMOUNT_TYPE']['VALUE'] ?></p>
                </div>
            <? endif; ?>

            <? /* Product quantity */
            if (!empty($arResult['PROPERTIES']['ITEMS_AMOUNT']['VALUE'])) :?>
                <div class="item-quantity specification">
                    <p><?= $arResult['PROPERTIES']['ITEMS_AMOUNT']['VALUE'] ?></p>
                </div>
            <? endif; ?>

            <div class="item-distance-date">
                <? if ($_COOKIE['userGeoposition']) : ?>
                    <div class="distance">
                        <? $item_coords = explode(',', $arResult['PROPERTIES']['YANDEX_MAP']['VALUE']);
                        $item_lat = trim($item_coords[0]);
                        $item_lon = trim($item_coords[1]);

                        $user_coord = explode('&', $_COOKIE['userGeoposition']);
                        $user_lat = trim($user_coord[0]);
                        $user_lon = trim($user_coord[1]);
                        $distance = getCoordDistance($item_lat, $item_lon, $user_lat, $user_lon);
                        $distance = round($distance);

                        if ($distance > 1000) {
                            if ($distance % 1000 == 0) {
                                echo $distance / 1000 . ' км';
                            } else {
                                $distance_metre = ($distance % 1000) / 100;
                                echo round($distance / 1000) . ' км ' . round($distance_metre) * 100 . ' м';
                            }
                        } else {
                            $distance_metre = $distance / 100;
                            echo round($distance_metre) * 100 . ' м';
                        } ?>
                    </div>
                <? endif; ?>
                <div class="date">
                    <?= getPublicationDate($arResult["PROPERTIES"]["LAST_UPDATE"]['VALUE'])?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="row element-info-bottom">
    <div class="col-md-6 slider-block">
        <?/*
        <? if (count($resultPictures) > 1) : ?>
            <div class="slide-controls-wrapper <?= (count($resultPictures) == 2 && count($resultPictures) > 1) ? 'two-pic' : 'more-two-pic' ?>">
                <ul class="slide-controls">
                    <? foreach ($resultPictures as $additionalPicture) : ?>
                        <li><img src="<?= $additionalPicture['resised_picture']['src'] ?>"
                                 origin-href=<?= $additionalPicture['original_picture']['src'] ?>></li>
                    <? endforeach; ?>
                </ul>
                <? if (count($resultPictures) > 3) : ?>
                    <p class="bx-button-nav">
                        <span id="slider-prev" class="col-xs-6"></span>
                        <span id="slider-next" class="col-xs-6"></span>
                    </p>
                <? endif; ?>
            </div>
        <? endif; ?>
        */?>
        <ul class="slider-thumbnails">
            <?for($i = 1; $i < 5; $i++):?>
                <li>
                    <? if($arResult['PROPERTIES']['ADDITIONAL_PHOTO']['VALUE'][$i]):?>
                        <?$pictureSmall = CFile::ResizeImageGet($arResult['PROPERTIES']['ADDITIONAL_PHOTO']['VALUE'][$i], array('width'=>'60', 'height'=>'65'), BX_RESIZE_IMAGE_EXACT, true);?>
                        <?$pictureBig = CFile::ResizeImageGet($arResult['PROPERTIES']['ADDITIONAL_PHOTO']['VALUE'][$i], array('width'=>'1024', 'height'=>'768'), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
                        <a class="fancybox-detail" rel="gallery"  href="<?=$pictureBig['src']?>">
                            <img src="<?= $pictureSmall['src'] ?>" alt=""/>
                        </a>
                     <? else :?>
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/catalog_icons/no-photo.jpg"/>
                    <? endif; ?>
                </li>
            <?endfor;?>
        </ul>

    </div>
    <? if (CUser::IsAuthorized()): ?>
        <div class="col-md-6 feedback-block">
            <a href="#feedback" class="btn btn-primary fancybox-detail">Написать продавцу</a>
        </div>
        <div style="display: none">
            <div class="feedback reviews-block" id="feedback">
                <form action="/account/messages/sendMessage.php" method="post">
                    <div class="form-title">
                        <p class="zagl">Написать продавцу</p>
                    </div>
                    <div class="form-body">
                        <p class="field">
                            <textarea name="review" placeholder="Текст сообщения"></textarea>
                        </p>
                        <input type="hidden" name="item_id" value="<?=$arResult['ID']?>">
                        <input type="hidden" name="user_id_from" value="<?=CUser::GetID()?>">
                        <input type="hidden" name="user_id_to" value="<?=$arResult['PROPERTIES']['ITEM_USER']['VALUE']?>">
                        <input type="hidden" name="backurl" value="<?=$APPLICATION->GetCurPage()?>">

                        <p class="button"><input class="btn" type="submit" value="Отправить"></p>
                    </div>
                </form>
            </div>
        </div>
    <?endif;?>
</div>
<div class="item-detail-text">
    <p class="detail-title">Описание товара</p>
    <? /* Detail text */
    if (!empty($arResult['DETAIL_TEXT'])) :?>
        <p class="detail-content"><?= $arResult['DETAIL_TEXT'] ?></p>
    <?else:?>
        <p class="detail-content">Описание товара отсутствует</p>
    <? endif; ?>
</div>
