<?php
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(CUser::IsAuthorized()){
    if($_POST['review']){
        CModule::IncludeModule("iblock");

        $iblock_id = 5;

        $el = new CIBlockElement;
        $PROP = array();

        $PROP[20] = strip_tags(trim($_POST['review'])); // Отзыв
        $PROP[21] = $_POST['stars'] ? $_POST['stars'] : ''; // Баллы
        $PROP[22] = $_POST['item_id']; // ID объявления
        $PROP[23] = CUser::GetID();// user id

        $arLoadProductArray = Array(
            "MODIFIED_BY"    => CUser::GetID(), // элемент изменен текущим пользователем
            "IBLOCK_SECTION_ID" => false,  // элемент лежит в корне раздела
            "IBLOCK_ID"      => $iblock_id,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $_POST['item_id'],
            "ACTIVE"         => "Y", // не активен
            "DATE_ACTIVE_FROM" => date("d.m.Y"),
        );
        $el->Add($arLoadProductArray);
        header('Location: '.$_POST['backurl'].'?message_success=Комментарий успешно добавлен');
    }
    elseif($_POST['backurl']){
        header('Location: '.$_POST['backurl'].'?message_error=Поле не может быть пустым');
    }
    else{
        header('Location: /404.php');
    }
}